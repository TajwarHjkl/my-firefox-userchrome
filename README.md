# Quick Guide

* Turning On Profile Customazation
	* On a Firefox tab, go to `about:config`.
	* Search preference name `toolkit.legacyUserProfileCustomizations.stylesheets`.
	* Set it to `True`.
* Creating the userChrome file
	*  Go to <kbd>☰ Menu</kbd>><kbd>Help</kbd>><kbd>More Troubleshooting Information</kbd>.
	*  You'll see a row with the title **Profile Directory**, click on <kbd>Open Directory</kbd>.
	*  In that Directory, create a new directory called `chrome`.
	*  Inside of `chrome` directory create a new file called `userChrome.css`.
*  Using This userChrome
	*  Replace (or copy-paste) your `userChrome.css` file with [userChrome.css](userChrome.css).

# Screenshots

![Screenshot 1](screenshot.png)

# Credits
See [Credits](CREDITS.md)

# See Also

* [My i3wm config](https://gitlab.com/TakeyTajwar/my-i3wm-config)
